#!/bin/sh

unzip -qo "$1" bass.txt
line=$(grep -e "^[0-9]\+.[0-9]\+.[0-9]\+ - [0-9]\+/[0-9]\+/[0-9]\+" bass.txt | head -1)
echo $line | sed "s, - [0-9]\+/[0-9]\+/[0-9]\+,,"
