import requests
import zipfile
import io
import re
import os
import hashlib

def identify_version(text):
    m = re.search('^(\d+.\d+(.\d+)?) - \d+/\d+/\d+', text, re.MULTILINE)
    return m.group(1)

def fetch_versions(url, platform):
    url = 'http://web.archive.org/web/timemap/link/{0}'.format(url)
    text = ''.join(requests.get(url).text.splitlines())
    links = requests.utils.parse_header_links(text)

    versions = {}

    for link in links:
        if 'memento' in link['rel'].split(' '):
            print(link['url'])
            r = requests.get(link['url'])

            zf = zipfile.ZipFile(io.BytesIO(r.content), 'r')
            txt = zf.read('bass.txt').decode('utf-8', errors='replace')
            version = identify_version(txt)
            md5 = hashlib.md5(r.content).hexdigest()

            v = versions.get(version)
            if v is not None:
                if v['md5'] == md5:
                    continue
                print('*** conflict, already found {0}'.format(version))
                v['count'] += 1
                versions[version] = v
                version = '{0}-{1}'.format(version, v['count'])
                print('new version tag: {0}'.format(version))
            else:
                versions[version] = {'md5': md5, 'count': 0}
                print('found {0}: {1}'.format(version, md5))

            filename = 'bass-{0}-{1}.zip'.format(version, platform)
            open(os.path.join('static', filename), 'wb').write(r.content)

fetch_versions('https://www.un4seen.com/files/bass24.zip', 'windows')
fetch_versions('https://www.un4seen.com/files/bass24-linux.zip', 'linux')
fetch_versions('https://www.un4seen.com/files/bass24-osx.zip', 'macos')
