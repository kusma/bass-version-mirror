---
Title: BASS version mirror
---
This is an archive of versioned releases of
<a href="https://www.un4seen.com/bass.html">BASS</a> for Windows,
Linux and macOS. All files have been retrieved from
<a href="http://web.archive.org/">web.archive.org</a>, and should
correspond to real, historical releases of BASS.

The purpose of this archive is to provide download links of specific
versions, to avoid unexpected changes to the content.
